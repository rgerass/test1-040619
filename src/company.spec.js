const company = require('./company');

it('Expect company id 3, 85 to match snapshot', () => {
    expect(company(3)).toMatchSnapshot();
    expect(company(85)).toMatchSnapshot();
});
it('Throw id needs to be integer error', () => {
    expect(() => {
        company('string');
    }).toThrow('id needs to be integer');
});
